## CONTENTS OF THIS FILE
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 
## INTRODUCTION

The Block photo gallery module allows users to customize the slider based on their needs by modifying CSS classes in view blocks under advanced section. 

 * For a full description of the module, visit the project page:
   [https://www.drupal.org/project/block_photo_gallery]

 * To submit bug reports and feature suggestions, or to track changes:
   [https://drupal.org/project/issues/block_photo_gallery]

## REQUIREMENTS

This module will not requires any dependencies.

## INSTALLATION
 
 * Install as you would normally install a contributed Drupal module. Visit:
   [https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules]
   for further information.

## CONFIGURATION
Content type, Views & Image styles these 3 will automatically created once the module was installed.

To add contents, Go to Administration » Content » Add content » Block Photo gallery.

To add blocks, Go to Administration » Structure » Block Layout.

Types of customization available in this module are:
- Single column slider without description content.
- Single column slider with description content floating over the gallery image.
- Single column slider with description content next to the gallery image.
- Multi column sliders (Two/three/four/five column sliders) with or without description text.
- Thumbnail slider

For detailed information, kindly check the documentation attached below.

Documentation: https://www.drupal.org/docs/8/modules/block-photo-gallery

## MAINTAINERS

Current maintainers:
 * [Rengaraj Srinivasan (rengaraj-srinivasan)](https://www.drupal.org//u/rengaraj95)


## This project has been sponsored by:
 * [Srijan Technologies](https://www.drupal.org/srijan-technologies)
