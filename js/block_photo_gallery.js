/**
 * Block photo gallery.
 */
 (function($, Drupal) {
    Drupal.behaviors.block_photo_gallery = {
        attach: function(context, settings) {
            $('.single-column .view-content').not('.slick-initialized').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                infinite: false,
                arrows: true,
                draggable: false,
                centerMode: false
            });

            $('.two-column .view-content').not('.slick-initialized').slick({ //Display slider in two column
                slidesToShow: 1.99,
                slidesToScroll: 1,
                autoplay: true,
                infinite: false,
                arrows: true,
                draggable: false,
                centerMode: false,
                responsive: [{
                    breakpoint: 557,
                    settings: {
                        slidesToShow: 1,
                    },
                }, ],
            });

            $('.three-column .view-content').not('.slick-initialized').slick({ //Display slider in three column
                slidesToShow: 2.99,
                slidesToScroll: 1,
                autoplay: true,
                infinite: false,
                arrows: true,
                draggable: false,
                centerMode: false,
                responsive: [{
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2,
                        },
                    },
                    {
                        breakpoint: 557,
                        settings: {
                            slidesToShow: 1,
                        },
                    },
                ],
            });

            $('.four-column .view-content').not('.slick-initialized').slick({ //Display slider in four column
                slidesToShow: 3.99,
                slidesToScroll: 1,
                autoplay: true,
                infinite: false,
                arrows: true,
                draggable: false,
                centerMode: false,
                responsive: [{
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2,
                        },
                    },
                    {
                        breakpoint: 557,
                        settings: {
                            slidesToShow: 1,
                        },
                    },
                ],
            });

            $('.five-column .view-content').not('.slick-initialized').slick({ //Display slider in five column
                slidesToShow: 4.99,
                slidesToScroll: 1,
                autoplay: true,
                infinite: false,
                arrows: true,
                draggable: false,
                centerMode: false,
                responsive: [{
                        breakpoint: 1199,
                        settings: {
                            slidesToShow: 3,
                        },
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2,
                        },
                    },
                    {
                        breakpoint: 557,
                        settings: {
                            slidesToShow: 1,
                        },
                    },
                ],
            });

            //Responsive thumbnail slider main / top content 
            $('.thumbnail-slider-top > .view-content').not('.slick-initialized').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                asNavFor: '.thumbnail-slider-top > .view-footer .thumbnail-slider-bottom > .view-content',
                autoplay: false,
                verticalSwiping: true,
                centerMode: false,
                speed: 11,
                fade: true,
                cssEase: 'linear',
                infinite: true,
                responsive: [{
                    breakpoint: 767,
                    settings: {
                        swipeToSlide: false,
                        draggable: false,
                        swipe: false,
                        autoplay: false,
                        verticalSwiping: false,
                        autoplay: false,
                        arrows: false,
                    }
                }]
            });

            //Responsive thumbnail slider bottom / thumbnail images 
            $('.thumbnail-slider-top > .view-footer .thumbnail-slider-bottom > .view-content').not('.slick-initialized').slick({
                slidesToShow: 9,
                slidesToScroll: 1,
                asNavFor: '.thumbnail-slider-top > .view-content',
                dots: false,
                centerMode: true,
                speed: 10,
                focusOnSelect: true,
                draggable: false,
                arrows: false,
                infinite: true,
                responsive: [{
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 7,
                            swipeToSlide: false,
                            draggable: false,
                            slidesToScroll: 1,
                            swipe: false,
                            autoplay: false,
                            verticalSwiping: false,
                        },
                    },
                    {
                        breakpoint: 500,
                        settings: {
                            slidesToShow: 4,
                            swipeToSlide: false,
                            draggable: false,
                            slidesToScroll: 1,
                            swipe: false,
                            autoplay: false,
                            verticalSwiping: false,
                        },
                    },
                ],
            });
            $('.block-gallery .views-field-field-image a').attr("target","_blank");
        }
    };
})(jQuery, Drupal);